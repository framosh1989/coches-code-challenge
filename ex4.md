# 4. Regando las plantas

Empieza la fiesta.

Tenemos que regar las `n` plantas de nuestro jardín. Las plantas están colocadas en una fila de 0 a `n-1` de izquierda a derecha. Hay un río en la posición -1 donde podemos rellenar cada vez que queramos nuestra regadera.

Cada planta requiere una cantidad específica de agua. Para regar a las plantas hay que hacer lo siguiente.

- Se riegan en orden de izquierda a derecha.
- Antes de regar cada planta, si no tuviesemos suficiente agua en la regadera, tendriamos que volver al río a rellenarla.
- No se puede rellenar antes de tiempo.

Inicialmente estás en el río (x=-1). Se necesita un paso para mover una unidad en el eje X.

Dado un array de enteros donde `plants[i]` es la cantidad de agua que necesita esa planta y `capacity` la capacidad de la regadera, devuelve el número de pasos para regar todas las plantas.

## Ejemplo 1 (se entenderá mejor)
>**Input**:  plants = [2,2,3,3], capacity = 5
>
> **Output**: 14
>
>**Explicación**:
>
> - Empezamos en el río con la regadera llena.
> 
> - Avanzamos a la planta 0 (1 paso) y la regamos. Nos quedan 3L en la regadera.
> 
> - Avanzamos a la planta 1 (1 paso) y la regamos. Nos queda 1L en la regadera.
>
> - Dado que no podemos regar la planta 2, volvemos al río a rellenar (2 pasos).
> 
> - Avanzar a la planta 2 y regar (3 pasos). Nos quedan 2L
>
> - Dado que no podemos regar la planta 3, volvermos al río a rellenar (3 pasos).
>
> - Avanzar a la planta 3 y regar (4 pasos):
> 
> PASOS NECESARIOS:  1 + 1 + 2 + 3 + 3 + 4 = 14


## Ejemplo 2
>**Input**:  plants = [1,1,1,4,2,3], capacity = 4
>
> **Output**: 30
>
>**Explicación**:
>
> - Regamos plantas 0, 1, y 2 (3 pasos). Volvemos al río (3 pasos).
>
> - Regamos planta 3 (4 pasos). Volvemos al río (4 pasos).
>
> - Regamos planta 4 (5 pasos). Volvemos al río (5 pasos).
>
> - Regamos planta 5 (6 pasos).
>
> PASOS NECESARIOS: 3 + 3 + 4 + 4 + 5 + 5 + 6 = 30


## Ejemplo 3
>**Input**:  plants = [7,7,7,7,7,7,7], capacity = 8
>
> **Output**: 49
>
>**Explicación**:
>
> Hay que volver a rellenar antes de regar cada planta
>
> PASOS NECESARIOS: 1 + 1 + 2 + 2 + 3 + 3 + 4 + 4 + 5 + 5 + 6 + 6 + 7 = 49