<?php

include 'ex1.php';

class ex1Test extends \PHPUnit\Framework\TestCase
{
    public function test1()
    {
        $this->assertEquals(true, iceCreamCart([5, 5, 5, 10, 20]));
    }

    public function test2()
    {
        $this->assertEquals(false, iceCreamCart([10]));
    }

    public function test3()
    {
        $this->assertEquals(false, iceCreamCart([5, 5, 10, 10, 20]));
    }

    public function test4()
    {
        $this->assertEquals(false, iceCreamCart([5, 5, 5, 10, 20, 20, 20, 20]));
    }

    public function test5()
    {
        $this->assertEquals(true, iceCreamCart([5, 5, 5, 10, 5, 20, 5, 10, 5, 20]));
    }

    public function test6()
    {
        $this->assertEquals(true, iceCreamCart([5, 5, 5, 5, 10, 5, 10, 10, 10, 20]));
    }

    public function test7()
    {
        $this->assertEquals(false, iceCreamCart([5, 5, 5, 5, 20, 20, 5, 5, 20, 5]));
    }
}
