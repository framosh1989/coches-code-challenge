<?php

include 'ex2.php';

class ex2Test extends \PHPUnit\Framework\TestCase
{
    public function test1()
    {
        $this->assertEquals(13, beerBottles(9, 3));
    }

    public function test2()
    {
        $this->assertEquals(19, beerBottles(15, 4));
    }

    public function test3()
    {
        $this->assertEquals(2, beerBottles(2, 3));
    }

    public function test4()
    {
        $this->assertEquals(7, beerBottles(6, 6));
    }

    public function test5()
    {
        $this->assertEquals(16, beerBottles(14, 7));
    }

    public function test6()
    {
        $this->assertEquals(22, beerBottles(19, 6));
    }
}
