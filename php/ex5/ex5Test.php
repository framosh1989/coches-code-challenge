<?php

include 'ex5.php';

class ex5Test extends \PHPUnit\Framework\TestCase
{
    public function test1()
    {
        $this->assertEquals(4, orangesRotting([
            [2, 1, 1],
            [1, 1, 0],
            [0, 1, 1]
        ]));
    }

    public function test2()
    {
        $this->assertEquals(-1, orangesRotting([
            [2, 1, 1],
            [0, 1, 1],
            [1, 0, 1]
        ]));
    }

    public function test3()
    {
        $this->assertEquals(-1, orangesRotting([
            [1],
        ]));
    }

    public function test4()
    {
        $this->assertEquals(4, orangesRotting([
            [2, 1, 1],
            [1, 1, 0],
            [0, 1, 1]
        ]));
    }

    public function test5()
    {
        $this->assertEquals(2, orangesRotting([
            [2, 1, 1],
            [1, 1, 1],
            [0, 1, 2]
        ]));
    }

    public function test6()
    {
        $this->assertEquals(2, orangesRotting([
            [1],
            [2],
            [1],
            [1]
        ]));
    }

    public function test7()
    {
        $this->assertEquals(0, orangesRotting([
            [0, 2]
        ]));
    }
}
