<?php

include 'ex4.php';

class ex4Test extends \PHPUnit\Framework\TestCase
{
    public function test1()
    {
        $this->assertEquals(14, wateringPlants([2,2,3,3], 5));
    }

    public function test2()
    {
        $this->assertEquals(30, wateringPlants([1,1,1,4,2,3], 4));
    }

    public function test3()
    {
        $this->assertEquals(49, wateringPlants([7,7,7,7,7,7,7], 8));
    }


    public function test4()
    {
        $this->assertEquals(16, wateringPlants([2,5,2,4], 5));
    }

    public function test5()
    {
        $this->assertEquals(30, wateringPlants([2,1,2,4,4,2], 5));
    }

    public function test6()
    {
        $this->assertEquals(5086, wateringPlants([1,4,5,3,1,3,2,5,1,4,2,3,5,1,1,5,3,3,1,5,2,3,2,4,5,3,1,5,5,2,5,5,2,3,5,5,3,1,3,2,5,5,1,1,5,5,2,2,4,2,1,3,3,3,4,3,3,3,2,1,5,1,5,5,5,1,3,1,2,5,4,3,3,3,1,2,2,2,3,4,4,3,1,5,2,3,3,2,3,2,5,4,2,1,1,1,5,5,4,2], 7));
    }
}
