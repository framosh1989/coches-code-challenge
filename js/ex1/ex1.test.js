const iceCreamCart = require('./ex1.js');

test('test1', () => {
  expect(iceCreamCart([5,5,5,10,20])).toBe(true);
});

test('test2', () => {
  expect(iceCreamCart([10])).toBe(false);
});

test('test3', () => {
  expect(iceCreamCart([5,5,10,10,20])).toBe(false);
});

test('test4', () => {
  expect(iceCreamCart([5,5,5,10,20,20,20,20])).toBe(false);
});

test('test5', () => {
  expect(iceCreamCart([5,5,5,10,5,20,5,10,5,20])).toBe(true);
});

test('test6', () => {
  expect(iceCreamCart([5,5,5,5,10,5,10,10,10,20])).toBe(true);
});

test('test7', () => {
  expect(iceCreamCart([5,5,5,5,20,20,5,5,20,5])).toBe(false);
});
