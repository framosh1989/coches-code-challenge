const orangesRotting = require('./ex5.js');

test('test1', () => {
    expect(orangesRotting(
        [
            [2, 1, 1],
            [1, 1, 0],
            [0, 1, 1]
        ]
    )).toBe(4);
});

test('test2', () => {
    expect(orangesRotting(
        [
            [2, 1, 1],
            [0, 1, 1],
            [1, 0, 1]
        ]
    )).toBe(-1);
});

test('test3', () => {
    expect(orangesRotting(
        [
            [1],
        ]
    )).toBe(-1);
});

test('test4', () => {
    expect(orangesRotting(
        [
            [2, 1, 1],
            [1, 1, 0],
            [0, 1, 1]
        ]
    )).toBe(4);
});

test('test5', () => {
    expect(orangesRotting(
        [
            [2, 1, 1],
            [1, 1, 1],
            [0, 1, 2]
        ]
    )).toBe(2);
});

test('test6', () => {
    expect(orangesRotting(
        [
            [1],
            [2],
            [1],
            [1]
        ]
    )).toBe(2);
});

test('test7', () => {
    expect(orangesRotting(
        [
            [0, 2]
        ]
    )).toBe(0);
});