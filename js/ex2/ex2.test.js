const beerBottles = require('./ex2.js');

test('test1', () => {
  expect(beerBottles(9, 3)).toBe(13);
});

test('test2', () => {
  expect(beerBottles(15, 4)).toBe(19);
});

test('test3', () => {
  expect(beerBottles(2, 3)).toBe(2);
});

test('test4', () => {
  expect(beerBottles(6, 6)).toBe(7);
});

test('test5', () => {
  expect(beerBottles(14, 7)).toBe(16);
});

test('test6', () => {
  expect(beerBottles(19, 6)).toBe(22);
});