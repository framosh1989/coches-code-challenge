const wateringPlants = require('./ex4.js');

test('test1', () => {
  expect(wateringPlants([2,2,3,3], 5)).toBe(14);
});

test('test2', () => {
  expect(wateringPlants([1,1,1,4,2,3], 4)).toBe(30);
});

test('test3', () => {
  expect(wateringPlants([7,7,7,7,7,7,7], 8)).toBe(49);
});

test('test4', () => {
  expect(wateringPlants([2,5,2,4], 5)).toBe(16);
});

test('test5', () => {
  expect(wateringPlants([2,1,2,4,4,2], 5)).toBe(30);
});

test('test6', () => {
  expect(wateringPlants([1,4,5,3,1,3,2,5,1,4,2,3,5,1,1,5,3,3,1,5,2,3,2,4,5,3,1,5,5,2,5,5,2,3,5,5,3,1,3,2,5,5,1,1,5,5,2,2,4,2,1,3,3,3,4,3,3,3,2,1,5,1,5,5,5,1,3,1,2,5,4,3,3,3,1,2,2,2,3,4,4,3,1,5,2,3,3,2,3,2,5,4,2,1,1,1,5,5,4,2], 7)).toBe(5086);
});