# Cochescom Code Challenge

## Problemas

1. [El carrito de los helados](ex1.md)
2. [Reciclando botellas de cerveza](ex2.md)
3. [La ecuación maldita](ex3.md)
4. [Regando las plantas](ex4.md)
5. [Naranjas podridas](ex5.md)

## ¿De que va esto?

Tenéis que resolver 5 problemas de programación por equipos. El equipo que resuelva antes los 5 problemas es el campeón y será alabado por el resto. Los problemas podéis resolverlos en PHP o JS, a vuestra elección.

## ¿Cómo lo hacemos?
Para cada uno de los 5 problemas, tenéis:
- el enunciado del problema
- una función vacía que tenéis que completar
- una suite de tests para validar que vuestra función es correcta. Evidentemente los tests ahora mismo fallan todos, tenéis que conseguir que pasen con vuestro talento.

Cuando tengais resueltos todos los problemas, crear una rama con vuestro número de equipo (teamX) y haced push en este repo.


## ¿Como hacerlo en PHP?

    cd php
    composer install
    composer tests1 //pasar los tests del ejercicio 1
    composer tests2 //ejercicio 2
    composer tests3 //ejercicio 3
    composer tests4 //ejercicio 4
    composer tests5 //ejercicio 5


## ¿Como hacerlo en JS?


    cd js
    npm install
    npm run tests1 //pasar los tests del ejercicio 1
    npm run tests2 //ejercicio 2
    npm run tests3 //ejercicio 3
    npm run tests4 //ejercicio 4
    npm run tests5 //ejercicio 4
    


