# 5. Naranjas podridas

Somos agricultores y nos dedicamos a cultivar naranjas.

Tenemos una matriz m x n donde cada celda puede tener uno de estos tres valores:

- 0 representa un hueco vacío

- 1 representa una naranja en buen estado

- 2 representa una naranja podrida

Cada minuto, cualquier naranja fresca que esté **adyacente en las 4 direcciones** (arriba, abajo, izquierda, derecha) a una naranja podrida, se pudrirá también. 

Devuelve los minutos que se tardaremos en no tener ninguna naranja fresca. Si es imposible, devuelve -1,


## Ejemplo 1 (se entenderá mejor)
>**Input**:  grid = 
> [
> 
> &nbsp;&nbsp;&nbsp;&nbsp;[2,1,1],
> 
> &nbsp;&nbsp;&nbsp;&nbsp;[1,1,0],
> 
> &nbsp;&nbsp;&nbsp;&nbsp;[0,1,1]
> 
>]
>
> **Output**: 4
>
>**Explicación gráfica**:
>
> ![image info](./images/img_ex5.png)


## Ejemplo 2        
>**Input**:  grid = [
>
>&nbsp;&nbsp;&nbsp;&nbsp;    [2,1,1],
>
>&nbsp;&nbsp;&nbsp;&nbsp;    [0,1,1],
>
>&nbsp;&nbsp;&nbsp;&nbsp;    [1,0,1]
> 
> ]
>
> **Output**: -1
>
>**Explicación**:
>
> La naranja de abajo a la izquierda nunca se pudrirá, dado que la celda de arriba y la de su derecha están vacias.

## Ejemplo 3     
>**Input**:  grid = [[0,2]]
>
> **Output**: 0
> 
> No hay naranjas frescas, la respuesta es 0.