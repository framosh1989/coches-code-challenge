
# 2. Reciclando botellas de cerveza

En Mercadona, para fomentar el reciclaje han sacado una promoción que si llevas `numExchange` botellines de cerveza vacíos, te regalan uno lleno.

Tienes `numBottles` botellines de cerveza llenos en tu nevera. ¿Cuántas botellas de cerveza podrás beberte en total dando los viajes a Mercadona que sean necesarios?

## Ejemplo 1

>**Input**: numBottles = 9, numExchange = 3
> 
> **Output**: 13
> 
>**Explicación**:
>
>- Tienes 9 botellas, te las bebes.
> 
>- Llevas las 9 botellas vacías a Mercadona, las entregas y te dan 3 llenas (1 por cada 3 vacías). Te las bebes.
> 
>- Llevas las 3 vacías y te dan 1, te la bebes.
>
> 
> 
>  Total: 9+3+1=13 botellas te bebes. Acabas borracho seguro.
> 
> ![image info](./images/img_ex2_1.png)

## Ejemplo 2
>**Input**: numBottles = 15, numExchange = 4
>
> **Output**: 19
>
>**Explicación**:
>
> Por cada vez que lleves 4, te dan una. 15 + 3 + 1 = 19.
>
>
> ![image info](./images/img_ex2_2.png)
