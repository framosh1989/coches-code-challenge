
# 1. El carrito de los helados

Tenemos un carrito de los helados y cada uno de nuestros helados cuesta 5€. Los clientes van viniendo en orden y compran un helado cada uno. Cada cliente solo comprará un **único** helado y nos pagará con un billete de 5€, 10€ o 20€. Tenemos que devolverle a cada cliente su cambio correcto para su pago de 5€.

Ten en cuenta que al empezar no tenemos ningún billete.

Dado un array de enteros donde bills[i] es el billete con el que nos paga el cliente i. Devuelva true si podemos devolver el cambio a todos los clientes. O false si en algún momento nos quedamos sin cambio.

## Ejemplo 1

> **Input:** bills = [5,5,5,10,20]
> 
> **Output:** true
> 
> **Explicación:**
> 
> Para los primeros 3 clientes, recogemos 3 billetes de 5€.
> 
> Para el cuatro cliente, recogemos un billete de 10€ y le devolvemos uno de 5€.
> 
> Para el último cliente, recogemos un billete de 20€ y le devolvemos uno de 10€ y uno de 5€
> 
> Dado que a todos los clientes le damos correctamente su cambio, devolvemos true.

## Ejemplo 2
> **Input:** bills = [5,5,10,10,20]
> 
> **Output:** false
> 
> **Explicación:**
> 
> Para los primeros 2 recogemos 2 billetes de 5€
> 
> Para el tercero, recogemos un billete de 10€ y le damos uno de 5€
> 
> Para el cuarto, recogemos un billete de 10€ y le damos uno de 5€.
> 
> Para el último (20€), solo tenemos dos billetes de 10€, por lo que no podemos devolverle su cambio, devolvemos false.
